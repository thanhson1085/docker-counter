FROM alpine:3.4

RUN apk add --no-cache python3 \
    && python3 -m ensurepip \
    && rm -r /usr/lib/python*/ensurepip \
    && pip3 install --upgrade pip setuptools \
    && rm -r /root/.cache

RUN pip3 install flask redis
COPY app /app
WORKDIR /app

EXPOSE 5000
CMD ["python3", "app.py"]
